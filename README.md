# Kafka Routing Slip PoC

#### What is this?
This will be a FOSS library for a Kafka stream framework that will look for a "routing slip" in the record headers of a kafka message, and use it to publish a message to that destination. The library should allow streams to "pass through" and not publish to the routing slip if they are not the last step in the processing chain.

**WIP**




### Kafka Bootstrapping
This is how you bring up and set up a local kafka cluster:
#### Run zookeeper docker
docker run -d --name zookeeper -p 2181:2181 confluent/zookeeper

#### Run Kafka docker that uses zookeeper (above)
docker run -d --name kafka -p 9092:9092 --env ADVERTISED_HOST=kafka --env ADVERTISED_PORT=9092 --link zookeeper:zookeeper confluent/kafka


#### Bring up tools docker
docker run -it -u root --network="host" --entrypoint bash confluent/kafka

#### useful commands
kafka-topics --zookeeper localhost:2181 --list
kafka-topics --zookeeper localhost:2181 --describe
kafka-topics --zookeeper localhost:2181 --describe --topic TOPIC_NAME
kafka-topics --zookeeper localhost:2181 --create --replication-factor 1 --partitions 3 --topic potato
