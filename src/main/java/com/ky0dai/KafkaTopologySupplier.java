package com.ky0dai;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;

import static java.util.regex.Pattern.compile;

public class KafkaTopologySupplier implements Supplier<Topology> {
    private static Pattern PATTERN = compile("\\W+", Pattern.UNICODE_CHARACTER_CLASS);
    private final String inputTopic;


    public KafkaTopologySupplier(String inputTopic) {
        this.inputTopic = inputTopic;
    }

    @Override
    public Topology get() {
        String outputTopic = "radish";
        Serde<String> stringSerde = Serdes.String();

        var builder = new StreamsBuilder();
        builder.stream(inputTopic)
                .flatMapValues(value -> split(value))
                .to(outputTopic);
//                .foreach((k, v) ->  System.out.println("word: " + k + " -> " + v)); // shortcut
//                .groupBy((k, w) -> w)
//                .count()
//                .toStream()
//                .foreach((word, count) -> System.out.println("word: " + word + " -> " + count));

        return builder.build();
    }

    private Iterable<?> split(Object value) {
        System.out.println("Received message: " + value);
        return Arrays.asList(PATTERN.split(value.toString()));
    }
}
