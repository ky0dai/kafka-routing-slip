package com.ky0dai;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import java.util.Properties;
import java.util.function.Supplier;

import static org.apache.kafka.streams.StreamsConfig.APPLICATION_ID_CONFIG;
import static org.apache.kafka.streams.StreamsConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.streams.StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG;
import static org.apache.kafka.streams.StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG;

public class KStreamConfigSupplier implements Supplier<Properties> {
    private final String topicName;
    private final String broker;

    public KStreamConfigSupplier(String topicName, String broker) {
        this.topicName = topicName;
        this.broker = broker;
    }

    @Override
    public Properties get() {
        var properties = new Properties();
        properties.put(BOOTSTRAP_SERVERS_CONFIG, broker);
        properties.put(APPLICATION_ID_CONFIG, topicName);
        properties.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        return properties;
    }
}
