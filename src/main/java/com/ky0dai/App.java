package com.ky0dai;

import org.apache.kafka.streams.KafkaStreams;

public class App
{
    public static final String TOPIC_NAME = "potato";
    public static final String BROKER = "localhost:9092";

    public static void main(String[] args ) throws InterruptedException {
        System.out.println( "Starting stream." );
        KafkaStreams stream = getStream();

        stream.start();

        Thread.sleep(60000);
        stream.close();
    }

    private static KafkaStreams getStream() {
        var configSupplier = new KStreamConfigSupplier(TOPIC_NAME, BROKER);
        var topologySupplier = new KafkaTopologySupplier(TOPIC_NAME);
        return new KafkaStreams(topologySupplier.get(), configSupplier.get());
    }

}
